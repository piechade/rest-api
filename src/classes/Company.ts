import { CompanyRepository } from '../repositories/CompanyRepository';
import { AppDAO } from '../dao';

export class Company {
  private _id: number | undefined;
  private _name: string | undefined;
  companyRepository: CompanyRepository;

  constructor(
    dao: AppDAO,
    params?: { id?: number | undefined; name?: string | undefined }
  ) {
    this.companyRepository = new CompanyRepository(dao);
    this._id = params!.id;
    this._name = params!.name;
  }

  set id(id) {
    this._id = id;
  }

  get id() {
    return this._id;
  }

  set name(name) {
    this._name = name;
  }
  get name() {
    return this._name;
  }

  static async init(dao: AppDAO) {
    return await new CompanyRepository(dao).createTable();
  }

  async save() {
    try {
      let result: any = await this.companyRepository.create({
        id: this.id,
        name: this.name
      });
      this._id = result.id;
      return result;
    } catch (error) {
      console.error(error.message);
      console.error(error.stack);
    }
  }

  async update() {
    try {
      return await this.companyRepository.update({
        id: this.id,
        name: this.name
      });
    } catch (error) {
      console.error(error.message);
      console.error(error.stack);
    }
  }

  async delete() {
    try {
      return await this.companyRepository.delete(this.id);
    } catch (error) {
      console.error(error.message);
      console.error(error.stack);
    }
  }

  static async getById(dao: AppDAO, id: number | undefined) {
    const companyRepository = new CompanyRepository(dao);
    const tempCompany: any = await companyRepository.getById(id);
    const newCompany = new Company(dao, {
      id: tempCompany.id,
      name: tempCompany.name
    });
    return newCompany;
  }

  static async getAll(dao: AppDAO) {
    const companyRepository = new CompanyRepository(dao);
    const tempCompanies: any = await companyRepository.getAll();
    let list: Array<Company> = [];

    for (let index = 0; index < tempCompanies.length; index++) {
      const element = tempCompanies[index];
      list.push(
        new Company(dao, {
          id: element.id,
          name: element.name
        })
      );
    }

    return list;
  }

  getJson() {
    return {
      company: {
        id: this.id,
        name: this.name
      }
  }
}
}
