import { PassengerRepository } from '../repositories/PassengerRepository';
import { AppDAO } from '../dao';

export class Passenger {
  private _id: number | undefined;
  private _name: string | undefined;
  passengerRepository: PassengerRepository;

  constructor(
    dao: AppDAO,
    params?: { id?: number | undefined; name?: string | undefined }
  ) {
    this.passengerRepository = new PassengerRepository(dao);
    this._id = params!.id;
    this._name = params!.name;
  }

  set id(id) {
    this._id = id;
  }

  get id() {
    return this._id;
  }

  set name(name) {
    this._name = name;
  }
  get name() {
    return this._name;
  }

  static async init(dao: AppDAO) {
    return await new PassengerRepository(dao).createTable();
  }

  async save() {
    try {
      let result: any = await this.passengerRepository.create({
        id: this._id,
        name: this.name
      });
      this._id = result.id;
      return result;
    } catch (error) {
      console.error(error.message);
      console.error(error.stack);
    }
  }

  async update() {
    try {
      return await this.passengerRepository.update({
        id: this._id,
        name: this.name
      });
    } catch (error) {
      console.error(error.message);
      console.error(error.stack);
    }
  }

  async delete() {
    try {
      return await this.passengerRepository.delete(this._id);
    } catch (error) {
      console.error(error.message);
      console.error(error.stack);
    }
  }

  static async getById(dao: AppDAO, id: number) {
    const passengerRepository = new PassengerRepository(dao);
    const tempPassenger: any = await passengerRepository.getById(id);
    const newPassenger = new Passenger(dao, {
      id: tempPassenger.id,
      name: tempPassenger.name
    });
    return newPassenger;
  }

  static async getAll(dao: AppDAO) {
    const passengerRepository = new PassengerRepository(dao);
    const tempPassengers: any = await passengerRepository.getAll();
    let list: Array<Passenger> = [];

    for (let index = 0; index < tempPassengers.length; index++) {
      const element = tempPassengers[index];
      list.push(
        new Passenger(dao, {
          id: element.id,
          name: element.name
        })
      );
    }

    return list;
  }
}
