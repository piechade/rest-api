import _ from 'lodash';
import { FlightRepository } from '../repositories/FlightRepository';
import { Company } from './Company';
import { Passenger } from './Passenger';
import { CompanyRepository } from '../repositories/CompanyRepository';
import { PassengerRepository } from '../repositories/PassengerRepository';
import { AppDAO } from '../dao';

export class Flight {
  private _id: number | undefined;
  private _company: Company | undefined;
  private _departureTime: Date | undefined;
  private _departureAirport: string | undefined;
  private _arrivalTime: Date | undefined;
  private _arrivalAirport: string | undefined;
  private _bookings: Array<{
    passanger: Passenger;
    place: string;
  }>;
  private _removedBookings: Array<{
    passanger: Passenger;
    place: string;
  }>;
  private flightRepository: FlightRepository;

  constructor(
    dao: AppDAO,
    params?:
      | {
          id?: number | undefined;
          company: Company | undefined;
          departureTime?: Date | undefined;
          departureAirport?: string | undefined;
          arrivalTime?: Date | undefined;
          arrivalAirport?: string | undefined;
        }
      | undefined
  ) {
    if (params != undefined) {
      this._id = params!.id;
      this._company = params!.company;
      this._departureTime = params!.departureTime;
      this._departureAirport = params!.departureAirport;
      this._arrivalTime = params!.arrivalTime;
      this._arrivalAirport = params!.arrivalAirport;
    }
    this._bookings = [];
    this._removedBookings = [];
    this.flightRepository = new FlightRepository(dao);
  }

  get id() {
    return this._id;
  }

  set id(id) {
    this._id = id;
  }

  get company() {
    return this._company;
  }
  set company(company) {
    this._company = company;
  }

  set departureTime(departureTime) {
    this._departureTime = departureTime;
  }
  get departureTime() {
    return this._departureTime;
  }

  set arrivalTime(arrivalTime) {
    this._arrivalTime = arrivalTime;
  }
  get arrivalTime() {
    return this._arrivalTime;
  }

  set departureAirport(departureAirport) {
    this._departureAirport = departureAirport;
  }
  get departureAirport() {
    return this._departureAirport;
  }

  set arrivalAirport(arrivalAirport) {
    this._arrivalAirport = arrivalAirport;
  }
  get arrivalAirport() {
    return this._arrivalAirport;
  }

  get bookings() {
    return this._bookings;
  }

  static async init(dao: AppDAO) {
    return await new FlightRepository(dao).createTable();
  }

  async save() {
    try {
      let result: any = await this.flightRepository.create({
        company: this.company,
        departureAirport: this.departureAirport,
        departureTime: this.departureTime,
        arrivalAirport: this.arrivalAirport,
        arrivalTime: this.arrivalTime
      });

      this._id = result.id;
      await this.updateBookings();

      return result;
    } catch (error) {
      console.error(error.message);
      console.error(error.stack);
    }
  }

  async update() {
    try {
      let result = await this.flightRepository.update({
        id: this._id,
        company: this.company,
        departureAirport: this.departureAirport,
        departureTime: this.departureTime,
        arrivalAirport: this.arrivalAirport,
        arrivalTime: this.arrivalTime
      });

      await this.updateBookings();

      return result;
    } catch (error) {
      console.error(error.message);
      console.error(error.stack);
    }
  }

  private async updateBookings() {
    for (let index = 0; index < this.bookings.length; index++) {
      const element = this.bookings[index];
      const tempBooking = await this.flightRepository.getBooking({
        flightId: this._id,
        passId: element.passanger.id,
        place: element.place
      });
      if (typeof tempBooking === 'undefined') {
        await this.flightRepository.addBooking({
          flightId: this._id,
          passId: element.passanger.id,
          place: element.place
        });
      }
    }
    for (let index = 0; index < this._removedBookings.length; index++) {
      const element = this._removedBookings[index];
      await this.flightRepository.removeBooking({
        flightId: this._id,
        passId: element.passanger.id,
        place: element.place
      });
    }
    this._removedBookings = [];
  }

  async delete() {
    try {
      return await this.flightRepository.delete(this._id);
    } catch (error) {
      console.error(error.message);
      console.error(error.stack);
    }
  }

  addBooking(passanger: Passenger, place: string) {
    this._removedBookings.splice(
      _.findIndex(this._removedBookings, function(item) {
        if (item.passanger.id === passanger.id && item.place === place) {
          return true;
        } else {
          return false;
        }
      }),
      1
    );
    const object = {
      passanger,
      place
    };

    if (!_.find(this._bookings, object)) {
      this._bookings.push(object);
    }
  }

  removeBooking(passanger: Passenger) {
    let context = this;
    this._bookings.splice(
      _.findIndex(this._bookings, function(item) {
        if (item.passanger.id === passanger.id) {
          context._removedBookings.push(item);
          return true;
        } else {
          return false;
        }
      }),
      1
    );
  }

  getJson() {
    let json = {
      flight: {
        id: this.id,
        company: {
          id: this.company!.id,
          name: this.company!.name
        },
        departureTime: this.departureTime,
        departureAirport: this.departureAirport,
        arrivalTime: this.arrivalTime,
        arrivalAirport: this.arrivalAirport,
        bookings: Array<{}>()
      }
    };

    for (let index = 0; index < this.bookings.length; index++) {
      const element = this.bookings[index];
      json.flight.bookings.push({
        passenger: {
          id: element.passanger.id,
          name: element.passanger.name
        },
        place: element.place
      });
    }
    return json;
  }

  static async getById(dao: AppDAO, id: number | undefined) {
    const flightRepository = new FlightRepository(dao);
    const tempFlight: any = await flightRepository.getById(id);
    const newCompany = await Company.getById(dao, tempFlight.id_comp);
    const newBookingList: any = await flightRepository.getBookings(
      tempFlight.id
    );
    const newFlight = new Flight(dao, {
      id: tempFlight.id,
      company: newCompany,
      departureTime: new Date(Date.parse(tempFlight.departureTime)),
      departureAirport: tempFlight.departureAirport,
      arrivalTime: new Date(Date.parse(tempFlight.arrivalTime)),
      arrivalAirport: tempFlight.arrivalAirport
    });
    for (let index = 0; index < newBookingList.length; index++) {
      const element: any = newBookingList[index];
      newFlight.addBooking(
        await Passenger.getById(dao, element.id_pass),
        element.place
      );
    }
    return newFlight;
  }

  static async getAll(dao: AppDAO) {
    const flightRepository = new FlightRepository(dao);
    const tempFlights: any = await flightRepository.getAll();
    let list: Array<Flight> = [];

    for (let index = 0; index < tempFlights.length; index++) {
      const element = tempFlights[index];
      const newCompany = await Company.getById(dao, element.id_comp);
      const newBookingList: any = await flightRepository.getBookings(
        element.id
      );
      const newFlight = new Flight(dao, {
        id: element.id,
        company: newCompany,
        departureTime: new Date(Date.parse(element.departureTime)),
        departureAirport: element.departureAirport,
        arrivalTime: new Date(Date.parse(element.arrivalTime)),
        arrivalAirport: element.arrivalAirport
      });
      for (let index = 0; index < newBookingList.length; index++) {
        const element: any = newBookingList[index];
        newFlight.addBooking(
          await Passenger.getById(dao, element.id_pass),
          element.place
        );
      }
      list.push(newFlight);
    }

    return list;
  }
}
