import { Router, Request, Response, NextFunction } from 'express';
import { Flight } from '../classes/Flight';
import { AppDAO } from '../dao';
import { Company } from '../classes/Company';
import { Passenger } from '../classes/Passenger';

export function run(dao: AppDAO): Router {
  const router: Router = Router();

  router.get('/', async (req: Request, res: Response) => {
    const flights = await Flight.getAll(dao);

    const result: Array<{
      id: any;
      company: any;
      departureTime: any;
      departureAirport: any;
      arrivalTime: any;
      arrivalAirport: any;
    }> = [];

    for (let index = 0; index < flights.length; index++) {
      const element = flights[index];
      result.push({
        id: element.id,
        company: {
          id: element.company!.id,
          name: element.company!.name
        },
        departureTime: element.departureTime,
        departureAirport: element.departureAirport,
        arrivalTime: element.arrivalTime,
        arrivalAirport: element.arrivalAirport
      });
    }

    res.status(200);
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(result));
  });

  router.post('/', async (req: Request, res: Response) => {
    const flight = new Flight(dao, {
      company: await Company.getById(dao, parseInt(req.body.CompanyId)),
      departureTime: req.body.departureTime,
      departureAirport: req.body.departureAirport,
      arrivalTime: req.body.arrivalTime,
      arrivalAirport: req.body.arrivalAirport
    });

    await flight.save();

    res.status(200);
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(flight.getJson()));
  });

  router.get('/:flightId', async (req: Request, res: Response) => {
    const flight = await Flight.getById(dao, req.params.flightId);

    res.status(200);
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(flight.getJson()));
  });

  router.put('/:flightId', async (req: Request, res: Response) => {
    const flight = await Flight.getById(dao, req.params.flightId);
    flight.company = await Company.getById(dao, parseInt(req.body.CompanyId));
    flight.departureTime = req.body.departureTime;
    flight.departureAirport = req.body.departureAirport;
    flight.arrivalTime = req.body.arrivalTime;
    flight.arrivalAirport = req.body.arrivalAirport;
    await flight.update();

    res.status(200);
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(flight.getJson()));
  });

  router.delete('/:flightId', async (req: Request, res: Response) => {
    const result = (await Flight.getById(dao, req.params.flightId)).delete();

    res.status(200);
    res.setHeader('Content-Type', 'application/json');
    res.send({ message: 'Successfully deleted Flight!' });
  });

  router.post('/book/:flightId', async (req: Request, res: Response) => {
    const flight = await Flight.getById(dao, req.params.flightId);
    flight.addBooking(
      await Passenger.getById(dao, req.body.passId),
      req.body.place
    );
    flight.update();
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(flight.getJson()));
  });

  router.put('/book/:flightId', async (req: Request, res: Response) => {
    const flight = await Flight.getById(dao, req.params.flightId);
    const pass = await Passenger.getById(dao, req.body.passId);
    //TODO: update booking
    flight.removeBooking(pass);
    flight.update();
    flight.addBooking(pass, req.body.place);
    flight.update();
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(flight.getJson()));
  });

  router.delete('/book/:flightId', async (req: Request, res: Response) => {
    const flight = await Flight.getById(dao, req.params.flightId);
    flight.removeBooking(await Passenger.getById(dao, req.body.passId));
    flight.update();
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(flight.getJson()));
  });

  return router;
}
