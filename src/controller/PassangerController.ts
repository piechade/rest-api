import { Router, Request, Response, NextFunction } from 'express';
import { Passenger } from '../classes/Passenger';
import { AppDAO } from '../dao';

export function run(dao: AppDAO): Router {
  const router: Router = Router();

  router.get('/', async (req: Request, res: Response) => {
    const passengers = await Passenger.getAll(dao);

    const result: Array<{ id: any; name: any }> = [];

    for (let index = 0; index < passengers.length; index++) {
      const element = passengers[index];
      result.push({
        id: element.id,
        name: element.name
      });
    }

    res.status(200);
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(result));
  });

  
  router.post('/', async (req: Request, res: Response) => {
    const passenger = new Passenger(dao, {
      name: req.body.name
    });

    await passenger.save();

    res.status(200);
    res.setHeader('Content-Type', 'application/json');
    res.send(
      JSON.stringify({
        passenger: {
          id: passenger.id,
          name: passenger.name
        }
      })
    );
  });

  router.get('/:passengerId', async (req: Request, res: Response) => {
    const passenger = await Passenger.getById(dao, req.params.passengerId);

    res.status(200);
    res.setHeader('Content-Type', 'application/json');
    res.send(
      JSON.stringify({
        passenger: {
          id: passenger.id,
          name: passenger.name
        }
      })
    );
  });

  router.put('/:passengerId', async (req: Request, res: Response) => {
    const passenger = await Passenger.getById(dao, req.params.passengerId);
    passenger.name = req.body.name;
    await passenger.update();

    res.status(200);
    res.setHeader('Content-Type', 'application/json');
    res.send(
      JSON.stringify({
        passenger: {
          id: passenger.id,
          name: passenger.name
        }
      })
    );
  });

  router.delete('/:passengerId', async (req: Request, res: Response) => {
    const result = (await Passenger.getById(dao, req.params.passengerId)).delete();

    res.status(200);
    res.setHeader('Content-Type', 'application/json');
    res.send({ message: 'Successfully deleted Passenger!' });
  });

  return router;
}
