import { Router, Request, Response, NextFunction } from 'express';
import { Company } from '../classes/Company';
import { AppDAO } from '../dao';

export function run(dao: AppDAO): Router {
  const router: Router = Router();

  router.get('/', async (req: Request, res: Response) => {
    const companies = await Company.getAll(dao);

    const result: Array<{ id: any; name: any }> = [];

    for (let index = 0; index < companies.length; index++) {
      const element = companies[index];
      result.push({
        id: element.id,
        name: element.name
      });
    }

    res.status(200);
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(result));
  });

  router.post('/', async (req: Request, res: Response) => {
    const company = new Company(dao, {
      name: req.body.name
    });

    await company.save();

    res.status(200);
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(company.getJson()));
  });

  router.get('/:companyId', async (req: Request, res: Response) => {
    const company = await Company.getById(dao, req.params.companyId);

    res.status(200);
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(company.getJson()));
  });

  router.put('/:companyId', async (req: Request, res: Response) => {
    const company = await Company.getById(dao, req.params.companyId);
    company.name = req.body.name;
    await company.update();

    res.status(200);
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(company.getJson()));
  });

  router.delete('/:companyId', async (req: Request, res: Response) => {
    const result = (await Company.getById(dao, req.params.companyId)).delete();

    res.status(200);
    res.setHeader('Content-Type', 'application/json');
    res.send({ message: 'Successfully deleted Company!' });
  });

  return router;
}
