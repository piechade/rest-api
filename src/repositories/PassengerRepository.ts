import { AppDAO } from '../dao';

export class PassengerRepository {
  dao: AppDAO;
  constructor(dao:AppDAO) {
    this.dao = dao;
  }

  createTable() {
    const sql = `
      CREATE TABLE IF NOT EXISTS passangers (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT)`;
    return this.dao.run(sql);
  }

  create(passenger: any) {
    const { id, name } = passenger;
    return this.dao.run(`INSERT INTO passangers (name) VALUES (?)`, [name]);
  }

  update(flight: any) {
    const { id, name } = flight;
    return this.dao.run(`UPDATE passangers SET name = ? WHERE id = ?`, [
      name,
      id
    ]);
  }

  delete(id: any) {
    return this.dao.run(`DELETE FROM passangers WHERE id = ?`, [id]);
  }

  getById(id: number) {
    return this.dao.get(`SELECT * FROM passangers WHERE id = ?`, [id]);
  }

  getAll() {
    return this.dao.all(`SELECT * FROM passangers`)
  }
}
