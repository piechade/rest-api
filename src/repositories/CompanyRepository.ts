import { AppDAO } from '../dao';

export class CompanyRepository {
  dao: AppDAO;
  constructor(dao:AppDAO) {
    this.dao = dao;
  }

  createTable() {
    const sql = `CREATE TABLE IF NOT EXISTS companies 
      (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT)`;
    return this.dao.run(sql);
  }

  create(company: any) {
    const { id, name } = company;
    return this.dao.run(`INSERT INTO companies (name) VALUES (?)`, [name]);
  }

  update(company: any) {
    const { id, name } = company;
    return this.dao.run(`UPDATE companies SET name = ? WHERE id = ?`, [name, id]);
  }

  delete(id: any) {
    return this.dao.run(`DELETE FROM companies WHERE id = ?`, [id]);
  }

  //To Do: change to number
  getById(id: any) {
    return this.dao.get(`SELECT * FROM companies WHERE id = ?`, [id]);
  }

  getAll() {
    return this.dao.all(`SELECT * FROM companies`)
  }
}
