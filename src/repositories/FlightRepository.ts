import { AppDAO } from '../dao';

export class FlightRepository {
  dao: AppDAO;
  constructor(dao:AppDAO) {
    this.dao = dao;
  }

  async createTable() {
    const sql = `
      CREATE TABLE IF NOT EXISTS flights (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        id_comp INTEGER,
        departureTime TEXT,
        arrivalTime TEXT,
        departureAirport TEXT,
        arrivalAirport TEXT)`
    const sql2 = `
        CREATE TABLE IF NOT EXISTS pass_in_flights (
          id_flight INTEGER,
          id_pass INTEGER,
          place TEXT)`

    const flights = await this.dao.run(sql)
    const pass = await this.dao.run(sql2)

    return { flights, pass }
  }

  create(flight:any) {
    if (flight.departureTime instanceof Date) {
      flight.departureTime = flight.departureTime.toISOString()
    }

    if (flight.arrivalTime instanceof Date) {
      flight.arrivalTime = flight.arrivalTime.toISOString()
    }

    return this.dao.run(
      `INSERT INTO flights (id_comp, departureTime, arrivalTime, departureAirport, arrivalAirport) VALUES (?, ?, ?, ?, ?)`, [flight.company.id, flight.departureTime, flight.arrivalTime, flight.departureAirport, flight.arrivalAirport])
  }

  update(flight:any) {
    if (flight.departureTime instanceof Date) {
      flight.departureTime = flight.departureTime.toISOString()
    }

    if (flight.arrivalTime instanceof Date) {
      flight.arrivalTime = flight.arrivalTime.toISOString()
    }

    const {
      id,
      company,
      departureTime,
      departureAirport,
      arrivalTime,
      arrivalAirport
    } = flight
    return this.dao.run(
      `UPDATE flights SET id_comp = ?, departureTime = ?, arrivalTime = ?, departureAirport = ?, arrivalAirport = ?  WHERE id = ?`, [company.id, departureTime, arrivalTime, departureAirport, arrivalAirport, id]
    )
  }

  delete(id:any) {
    return this.dao.run(
      `DELETE FROM flights WHERE id = ?`, [id]
    )
  }

  //To Do: change to number
  getById(id:any) {
    return this.dao.get(
      `SELECT * FROM flights WHERE id = ?`, [id])
  }

  getAll() {
    return this.dao.all(`SELECT * FROM flights`)
  }

  addBooking(object:any) {
    const {
      flightId,
      passId,
      place
    } = object
    return this.dao.run(
      `INSERT INTO pass_in_flights (id_flight, id_pass, place) VALUES (?, ?, ?)`, [flightId, passId, place])
  }

  getBooking(object:any) {
    const {
      flightId,
      passId,
      place
    } = object
    return this.dao.get(
      `SELECT * FROM  pass_in_flights WHERE id_flight = ? AND id_pass = ? AND place = ?`, [flightId, passId, place])
  }

  removeBooking(object:any) {
    const {
      flightId,
      passId,
      place
    } = object
    return this.dao.run(
      `DELETE FROM pass_in_flights WHERE id_flight = ? AND id_pass = ? AND place = ?`, [flightId, passId, place])
  }

  getBookings(FlightId:number) {
    return this.dao.all(
      `SELECT * FROM  pass_in_flights WHERE id_flight = ?`, [FlightId])
  }
}
