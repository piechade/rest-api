import express = require('express');
import * as bodyParser from 'body-parser';
import { Routes } from './routes';
import { AppDAO } from './dao';
import { Company } from './classes/Company';
import { Passenger } from './classes/Passenger';
import { Flight } from './classes/Flight';

class App {
  public app: express.Application;
  public routePrv: Routes = new Routes();
  public sqlUrl: string = './database.sqlite3';
  private dao:AppDAO

  constructor() {
    this.dao;
    this.app = express();
    this.config();
    this.sqlSetup();
    this.routePrv.routes(this.app, this.dao);
  }

  private config(): void {
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false }));
  }

  private sqlSetup() {
    this.dao = new AppDAO('./database.sqlite3');
    try {
      Company.init(this.dao);
      Passenger.init(this.dao);
      Flight.init(this.dao);
    } catch (error) {
      console.error(error.message);
      console.error(error.stack);
    }
  }
}

export default new App().app;
