import { Application, Router, Request, Response } from 'express';
import * as CompanyController from './controller/CompanyController';
import * as PassangerController from './controller/PassangerController';
import * as FlightController from './controller/FlightController';
import { AppDAO } from './dao';

export class Routes {
  public routes(app: Application, dao: AppDAO) {
    const router: Router = Router();

    router.get('/', (req: Request, res: Response) => {
      res.status(200).send({
        message: 'Welcome to Flight API!'
      });
    });

    app.use('/', router);
    app.use('/company', CompanyController.run(dao));
    app.use('/passanger', PassangerController.run(dao));
    app.use('/flight', FlightController.run(dao));
  }
}
