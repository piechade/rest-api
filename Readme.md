# Flight Booking System

## Install and run the server

Install dependencies:
````
npm install
````
Run server:
````
npm start
````

## API routes

### **Company**

### **Show Companies**
----
  Returns json data about all Companies.

* **URL**

  /company

* **Method:**

  `GET`

* **Success Response:**

  * **Code:** 200    
    **Content:** `[
    {
        "id": 1,
        "name": "TUI"
    }
]`

### **Create Company**
----
  Returns json data about a company.

* **URL**

  /company/

* **Method:**

  `POST`
  
*  **Data Params**

   **Required:**
 
   `name=[string]`

* **Success Response:**

  * **Code:** 200    
    **Content:** `{
    "company": {
        "id": 1,
        "name": "TUI"
    }
}`

### **Show Company**
----
  Returns json data about a company.

* **URL**

  /company/:id

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   `id=[number]`

* **Success Response:**

  * **Code:** 200    
    **Content:** `{
    "company": {
        "id": 1,
        "name": "TUI"
    }
}`

### **Update Company**
----
  Returns json data about a company.

* **URL**

  /company/:id

* **Method:**

  `PUT`
  
*  **URL Params**

   **Required:**
 
   `id=[number]`

*  **Data Params**

   **Required:**
 
   `name=[string]`

* **Success Response:**

  * **Code:** 200    
    **Content:** `{
    "company": {
        "id": 1,
        "name": "TUI"
    }
}`

### **Delete Company**
----
  Returns json data about a company.

* **URL**

  /company/:id

* **Method:**

  `DELETE`
  
*  **URL Params**

   **Required:**
 
   `id=[number]`

* **Success Response:**

  * **Code:** 200    
    **Content:** `{
    "company": {
        "id": 1,
        "name": "TUI"
    }
}`


### **Passenger**

### **Show Passengers**
----
  Returns json data about all Passengers.

* **URL**

  /passenger

* **Method:**

  `GET`

* **Success Response:**

  * **Code:** 200    
    **Content:** `[
    {
        "id": 1,
        "name": "Max Mustermann"
    }
]`

### **Create Passenger**
----
  Returns json data about a passenger.


* **URL**

  /passenger/

* **Method:**

  `POST`
  
*  **Data Params**

   **Required:**
 
   `name=[string]`

* **Success Response:**

  * **Code:** 200    
    **Content:** `{
    "passenger": {
        "id": 1,
        "name": "Max Mustermann"
    }
}`

### **Show Passanger**
----
  Returns json data about a passanger.

* **URL**

  /passanger/:id

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   `id=[number]`

* **Success Response:**

  * **Code:** 200    
    **Content:** `{
    "passanger": {
        "id": 1,
        "name": "Max Mustermann"
    }
}`

### **Update Company**
----
  Returns json data about a passanger.

* **URL**

  /passanger/:id

* **Method:**

  `PUT`
  
*  **URL Params**

   **Required:**
 
   `id=[number]`

*  **Data Params**

   **Required:**
 
   `name=[string]`

* **Success Response:**

  * **Code:** 200    
    **Content:** `{
    "passanger": {
        "id": 1,
        "name": "Max Mustermann"
    }
}`

### **Delete Passanger**
----
  Returns json data about a passanger.

* **URL**

  /passanger/:id

* **Method:**

  `DELETE`
  
*  **URL Params**

   **Required:**
 
   `id=[number]`

* **Success Response:**

  * **Code:** 200    
    **Content:** `{
    "passanger": {
        "id": 1,
        "name": "Max Mustermann"
    }
}`


### **Flight**

### **Show Flights**
----
  Returns json data about all flights.

* **URL**

  /flight

* **Method:**

  `GET`

* **Success Response:**

  * **Code:** 200    
    **Content:** 
    ```[
    {
        "id": 1,
        "company": {
            "id": 2,
            "name": "TUI"
        },
        "departureTime": "2018-06-06T13:34:47.559Z",
        "departureAirport": "test1",
        "arrivalTime": "2018-06-06T13:34:47.559Z",
        "arrivalAirport": "test2"
    }
]```

### **Create Flight**
----
  Returns json data about a passenger.


* **URL**

  /flight

* **Method:**

  `POST`
  
*  **Data Params**

   **Required:**
 
   `name=[string]`   
   `CompanyId=[number]`   
   `departureTime=[string]` (ISO date string)   
   `departureAirport=[string]`   
   `arrivalTime=[string]`   (ISO date string)   
   `arrivalAirport=[string]`   

* **Success Response:**

  * **Code:** 200    
    **Content:** 
    ```{
    "flight": {
        "id": 2,
        "company": {
            "id": 2,
            "name": "TUI"
        },
        "departureTime": "2018-06-06T13:34:47.559Z",
        "departureAirport": "test1",
        "arrivalTime": "2018-06-06T13:34:47.559Z",
        "arrivalAirport": "test2",
        "bookings": []
    }
}```

### **Show Flight**
----
  Returns json data about a flight.

* **URL**

  /flight/:id

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   `id=[number]`

* **Success Response:**

  * **Code:** 200    
    **Content:** 
    ```{
    "flight": {
        "id": 2,
        "company": {
            "id": 2,
            "name": "TUI"
        },
        "departureTime": "2018-06-06T13:34:47.559Z",
        "departureAirport": "test1",
        "arrivalTime": "2018-06-06T13:34:47.559Z",
        "arrivalAirport": "test2",
        "bookings": []
    }
}```

### **Update Flight**
----
  Returns json data about a flight.

* **URL**

  /passanger/:id

* **Method:**

  `PUT`
  
*  **URL Params**

   **Required:**
 
   `id=[number]`

*  **Data Params**

   **Required:**
 
   `name=[string]`   
   `CompanyId=[number]`   
   `departureTime=[string]` (ISO date string)   
   `departureAirport=[string]`   
   `arrivalTime=[string]`   (ISO date string)   
   `arrivalAirport=[string]`   

* **Success Response:**

  * **Code:** 200    
    **Content:** 
    ```{
    "flight": {
        "id": 2,
        "company": {
            "id": 2,
            "name": "TUI"
        },
        "departureTime": "2018-06-06T13:34:47.559Z",
        "departureAirport": "test1",
        "arrivalTime": "2018-06-06T13:34:47.559Z",
        "arrivalAirport": "test2",
        "bookings": []
    }
}```

### **Delete Flight**
----
  Returns json data about a flight.

* **URL**

  /passanger/:id

* **Method:**

  `DELETE`
  
*  **URL Params**

   **Required:**
 
   `id=[number]`

* **Success Response:**

  * **Code:** 200    
    **Content:** 
    ```{
    "flight": {
        "id": 2,
        "company": {
            "id": 2,
            "name": "TUI"
        },
        "departureTime": "2018-06-06T13:34:47.559Z",
        "departureAirport": "test1",
        "arrivalTime": "2018-06-06T13:34:47.559Z",
        "arrivalAirport": "test2",
        "bookings": []
    }
}```

### **Create Flight Booking**
----
  Returns json data about a flight.


* **URL**

  /flight/book/:id

* **Method:**

  `POST`

*  **URL Params**

   **Required:**
 
   `id=[number]`
  
*  **Data Params**

   **Required:**
  
   `passId=[number]`   
   `place=[string]`  

* **Success Response:**

  * **Code:** 200    
    **Content:** 
    ```{
    "flight": {
        "id": 1,
        "company": {
            "id": 2,
            "name": "TUI"
        },
        "departureTime": "2018-06-06T13:34:47.559Z",
        "departureAirport": "test1",
        "arrivalTime": "2018-06-06T13:34:47.559Z",
        "arrivalAirport": "test2",
        "bookings": [
            {
                "passenger": {
                    "id": 1,
                    "name": "passanger"
                },
                "place": "A1"
            }
        ]
    }
}```

### **Update Flight Booking**
----
  Returns json data about a flight.

* **URL**

  /flight/book/:id

* **Method:**

  `PUT`
  
*  **URL Params**

   **Required:**
 
   `id=[number]`
  
*  **Data Params**

   **Required:**
  
   `passId=[number]`   
   `place=[string]`   

* **Success Response:**

  * **Code:** 200    
    **Content:** 
    ```{
    "flight": {
        "id": 1,
        "company": {
            "id": 2,
            "name": "TUI"
        },
        "departureTime": "2018-06-06T13:34:47.559Z",
        "departureAirport": "test1",
        "arrivalTime": "2018-06-06T13:34:47.559Z",
        "arrivalAirport": "test2",
        "bookings": [
            {
                "passenger": {
                    "id": 1,
                    "name": "passanger"
                },
                "place": "A1"
            }
        ]
    }
}```

### **Delete Flight Booking**
----
  Returns json data about a flight.

* **URL**

  /flight/book/:id

* **Method:**

  `DELETE`
  
*  **URL Params**

   **Required:**
 
   `id=[number]`

* **Success Response:**

  * **Code:** 200    
    **Content:**
    ```{
    "flight": {
        "id": 1,
        "company": {
            "id": 2,
            "name": "TUI"
        },
        "departureTime": "2018-06-06T13:34:47.559Z",
        "departureAirport": "test1",
        "arrivalTime": "2018-06-06T13:34:47.559Z",
        "arrivalAirport": "test2",
        "bookings": []
    }
}```